// Introduction to Postman and REST

/*  OBJECTIVES

		- understand what REST is
		- use of POSTMAN 



	What is an API? 
		( Application Programming Interface )
			- the part of a server resposible for receiving requests and sending responses
			- ex. bank teller stands between customer and bank's vault, an API acts as the middleman between the front and back ends of an application

			diagram:
				Application ->	Interface 	-> Programming
								  (API)

		What is it for?
			- HIDES the server beneath an interface layer
			- hidden complexity makes apps easier to use


	What is REST?
		- an architectural style 
		- the need to seperate user interface concerns of the client from data storage concerns of the server

			// How does it solve the problem?
				1. Statelessness 
						- server does not need to know client state and vice versa
						- every client request has all the information it needs to be processed by the API
						- made possible via resources and HTTP

				2. Standardized Communication
						- enables a decoupled server (process of minimizing of one system to another) to understand, process, and respond to client requests WITHOUT knowing client state
						- implemented via resources represented as Unifor Resource Identifier (URI) endpoints and HTTP methods

	Anatomy of Client Request
		- HTTP methods and corresponding CRUD operations

	in postman HEADER:
		- it contains addtional request information

	in postman BODY:
		- contains data (always in RAW and JSON file)

*/







// JAVASCRIPT SYCHRONOUS VS ASYCHRONOUS

/* 
	external links:
		https://www.freecodecamp.org/news/synchronous-vs-asynchronous-in-javascript/

		https://jsonplaceholder.typicode.com/posts

		https://jsonplaceholder.typicode.com/guide/


	Synchronous
		- it runs the code one at a time
		- operataions tasks are performed one at a time and only when one is completed, the following is unblocked
			- "BLOCKING" is when the execution of additional JS Process must wait until the operation completes
		- hence JavaScript is by default is SYNCHRONOUS 
*/
		console.log('Hello World') // <- run 1st
		console.log('Hello Again') // <- run 2nd

		// so once an error is caught in the 1st line, the program wont execute, and thats the synchronous operation

		
/* Asynchronous
		- 2 primary triggers:

	1. browser API/WEB API events or functions. Theses include methods like setTimeout, or event handlers like onclick, mouse over, scroll and many more

	example:
*/
/*
		function printMe () {
			console.log('Print Me');
		}

		// printing after 5sec
		setTimeout(printMe, 5000) 
		// this will print first before printMe
		console.log('sample')
*/
/*
	// sample 2:

		function f1() {
			console.log('f1')
		}

		function f2() {
			console.log('f2')
		}

		function main() {
			console.log('main')

			setTimeout(f1, 0)

			// Promise > setTimeout
			new Promise((resolve, reject) =>
				resolve('I am a promise')
				).then(resolve => console.log(resolve))

			f2()
		}

		main();

		// output: main -> f2 -> promise -> f1
				// even 0sec, async is triggered hence f1 has delayed
*/

/*
	CALLBACK QUEUE (Data Structure)
		- in JS, we have a queue of callback queue or task queue. A queue data structure is "First-In-First-Out"

	JOB QUEUE
		- everytime a promise occurs in the code, the executor function gets into the job queue. The event loop workds as useual, to look into the queues but gives priority to the job queue items over the callnack queue items when the stack is free.
	
	2. PROMISE
			- a unique javascript object that allows us to perform asynchronous
			- the promise object represents the eventual completion (or failure) of an asynchronous operation and its resulting value
	

		new Promise((resolve, reject) =>
			resolve('I am a promise')
		).then(resolve => console.log(resolve))

*/



/* REST API using a jsonplaceholder

	// getting all posts (GET Method)

	// Fetch -> allows us to asynchronously request for a resource (data)
			 -> We use the promise for async
			 -> The Fetch API provides a JavaScript interface for accessing and manipulating parts of the HTTP pipeline, such as requests and responses

			syntax:

			fetch('url', {optional objects eg. GET, POST, etc})
				.then(response => response.json())
				.then(data)

		NOTE: optional objects - which contains information about our request such as the method, the body,  and the headers of our request.
*/

	console.log(fetch('https://jsonplaceholder.typicode.com/posts'))
	// A promise may be in one of 3 possible states: pending, fulfilled or rejected

	fetch('https://jsonplaceholder.typicode.com/posts')
		.then(response => response.json()) //parse the response as JSON
		.then(data => {
			console.log(data) // process the results
		})


	 //USING async await function

		async function fetchData() {
			// wait for the 'Fetch' method to complete then stores the value in a variable name
			let result = await fetch('https://jsonplaceholder.typicode.com/posts')
			console.log(result)
			console.log(typeof result)

			// converts the data from the 'response'
			let json = await result.json()

			console.log(json)
			console.log(typeof json)
		}

		fetchData();

	

	// Retrieving a specific post
	fetch('https://jsonplaceholder.typicode.com/posts/1')
		.then(response => response.json()) //parse the response as JSON
		.then(data => {
			console.log(data) // process the results
		})


	// Creates a new post 
	fetch('https://jsonplaceholder.typicode.com/posts', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		// sets the content/body of the 'request' object to be sent to the backend
		body: JSON.stringify({
			title: 'New Post',
			body: 'Hello Again',
			userId: 2
		})
	})
	.then(res => res.json())
	.then(data => console.log(data))



	// Updating a Post
	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json'
		},
		// sets the content/body of the 'request' object to be sent to the backend
		body: JSON.stringify({
			title: 'My First Revised Blog Post',
			body: 'Hello there! I revised this a bit',
			userId: 1
		})
	})
	.then(res => res.json())
	.then(data => console.log(data))

	// PUT Method - updates the whole document. Modifying resource where the client send data that updates the ENTRIRE resource.

	//while,

	// PATCH Method - applies partial updates to the resource

	// DELETING Method
	fetch('https://jsonplaceholder.typicode.com/posts/1', {method: 'DELETE'})
		.then(response => response.json()) 
		.then(data => 
			console.log(data)
		)


	// Filtering Posts
	// Information sent via the url can be done by adding the question mark symbol (?)

	/*

	Syntax:
		Individual Parameters
			'url? <parameterName> = <value>'

		Multiple Parameters
			'url? <parameterName1> = <valueA> & <parameterName2>= <valueB>

	*/


	fetch('https://jsonplaceholder.typicode.com/posts?userId=1')
	.then(res => res.json())
	.then(data => console.log(data))


	// Retrieve a nested/related comments to posts

	// Retrieving comments for a specific post (/post/:id/comments)

	fetch('https://jsonplaceholder.typicode.com/posts/2/comments')
	.then(res => res.json)
	.then(data => console.log(data))

