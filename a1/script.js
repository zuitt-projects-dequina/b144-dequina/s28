// S28 Activity

// API Placeholder : https://jsonplaceholder.typicode.com/todos


// #3

fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => response.json())
.then(data => {
	console.log(data)
})

// #4

fetch('https://jsonplaceholder.typicode.com/todos?userId=1')
.then(response => response.json())
.then(data => {

		let allTitle = data.map(title => {
			return `${title.id}. ${title.title}`;
		})
		let arrTitle = [allTitle];
		console.log(arrTitle)
	})


// #5

fetch('https://jsonplaceholder.typicode.com/todos?id=1')
.then(response => response.json())
.then(data => {
		
		let singleTodo = data.map(todo => {
			return `Title: ${todo.title}
			 
			Status: ${todo.completed}`;
		})
		
		console.log(singleTodo)
	})


// #6
fetch('https://jsonplaceholder.typicode.com/todos?id=1')
.then(response => response.json())
.then(data => {
		
		let singleTodo = data.map(todo => {
			return `Title: ${todo.title}
			 
			Status: ${todo.completed}`;
		})
		
		console.log(singleTodo)
	})


// #7

fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	
	body: JSON.stringify({
		title: 'New To-Do',
		completed: false,
		userId: 1
	})
})
.then(res => res.json())
.then(data => console.log(data))

// #8

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	// sets the content/body of the 'request' object to be sent to the backend
	body: JSON.stringify({
		title: 'My New To-Do',
		completed: false,
		userId: 1
	})
})
.then(res => res.json())
.then(data => console.log(data))


// #9

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	// sets the content/body of the 'request' object to be sent to the backend
	body: JSON.stringify({
		title: 'Another To-Do',
		description: 'Must do. Cant procrastinate',
		status: 'completed',
		dataCompleted: 'December 3',
		userId: 9
	})
})
.then(res => res.json())
.then(data => console.log(data))


// #10

fetch('https://jsonplaceholder.typicode.com/todos/1', {
		method: 'PATCH',
		headers: {
			'Content-Type': 'application/json'
		},
		// sets the content/body of the 'request' object to be sent to the backend
		body: JSON.stringify({
			title: 'Revision Revision'
		})
	})
	.then(res => res.json())
	.then(data => console.log(data))


// #11


fetch('https://jsonplaceholder.typicode.com/todos/1', {
		method: 'PATCH',
		headers: {
			'Content-Type': 'application/json'
		},
		// sets the content/body of the 'request' object to be sent to the backend
		body: JSON.stringify({
			completed: true
		})
	})
	.then(res => res.json())
	.then(data => console.log(data))

// #12

fetch('https://jsonplaceholder.typicode.com/todos/1', {method: 'DELETE'})
	.then(response => response.json()) 
	.then(data => 
		console.log(data)
	)


// #13
	// @postman
